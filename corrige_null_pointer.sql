SET SERVEROUTPUT ON SIZE UNLIMITED;
DECLARE        

		l_programas_lista_id VARCHAR(255);
		l_programas_lista_nome VARCHAR(255);    
				
		--Projetos
		CURSOR c1 IS
				SELECT DISTINCT 
							 r.request_id, 
							 r.description,                               
							 lpe.lifecycle_id,
							 pgmc.program_id AS content_program_id,
							 pgm.program_id,
							 pgm.container_name as program_name,
							 lpe.active_entity,
							 fppj.prj_program_id as proj_lista_programas_id,
							 fppj.prj_program_name as proj_lista_programas_nome,
							 pgm_proj.program_id AS proj_pgm_id,
							 fpp.prop_program_id as prop_lista_programas_id,
							 fpp.prop_program_name as prop_lista_programas_nome,
							 pgm_prop.program_id AS prop_pgm_id
					FROM pfm_lifecycle_parent_entity lpe
							 JOIN kcrt_requests r
								 ON r.request_id = DECODE(lpe.active_entity,'Proposal',lpe.proposal_req_id,'Project',lpe.project_req_id,'Asset',lpe.asset_req_id)                                   
							 JOIN kcrt_req_header_details rhd
								 ON rhd.request_id = r.request_id                  
					LEFT JOIN pgm_program_content pgmc
								 ON pgmc.content_id = lpe.lifecycle_id
					LEFT JOIN pgm_programs pgm
								 ON pgm.program_id = pgmc.program_id                    
					LEFT JOIN kcrt_fg_pfm_project fppj
								 ON lpe.active_entity = 'Project'
										AND fppj.request_id = lpe.project_req_id
					LEFT JOIN kcrt_fg_pfm_proposal fpp
								 ON lpe.active_entity = 'Proposal'
										AND fpp.request_id = lpe.project_req_id
					LEFT JOIN pgm_programs pgm_proj
								 ON '#@#' || fppj.prj_program_id || '#@#' LIKE '%#@#' || pgm_proj.program_id || '#@#%'
					LEFT JOIN pgm_programs pgm_prop
								 ON '#@#' || fpp.prop_program_id || '#@#' LIKE '%#@#' || pgm_prop.program_id || '#@#%'
				 WHERE lpe.active_entity IN ('Proposal','Project')          
							 AND ((fppj.prj_program_id IS NOT NULL AND pgm_proj.program_id IS NULL)
										OR (fpp.prop_program_id IS NOT NULL AND pgm_prop.program_id IS NULL)
										OR ('#@#' || fpp.prop_program_id || '#@#' NOT LIKE '%#@#' || pgm.program_id || '#@#%')
										OR ('#@#' || fppj.prj_program_id || '#@#' NOT LIKE '%#@#' || pgm.program_id || '#@#%'))
				ORDER BY r.description,pgm.program_id ASC;
								 
BEGIN    
				
FOR c1_rec IN c1
		LOOP                        

				SELECT LISTAGG(pgm.program_id,'#@#') WITHIN GROUP (ORDER BY pgm.program_id),
							 LISTAGG(pgm.container_name,'#@#') WITHIN GROUP (ORDER BY pgm.program_id)
					INTO l_programas_lista_id,
							 l_programas_lista_nome
					FROM pgm_programs pgm
							 JOIN pgm_program_content pgmc
								 ON pgmc.program_id = pgm.program_id
				 WHERE pgmc.content_id = c1_rec.lifecycle_id;                    

				IF c1_rec.active_entity = 'Project' THEN
					UPDATE kcrt_fg_pfm_project
							 SET prj_program_id = l_programas_lista_id,
								 prj_program_name = l_programas_lista_nome
					 WHERE request_id = c1_rec.request_id;
				ELSIF c1_rec.active_entity = 'Proposal' THEN
					UPDATE kcrt_fg_pfm_proposal
							 SET prop_program_id = l_programas_lista_id,
									 prop_program_name = l_programas_lista_nome
					 WHERE request_id = c1_rec.request_id;
				END IF;          
			 
				UPDATE kcrt_requests
					 SET last_update_date = sysdate,
							 last_updated_by = 1
				 WHERE request_id = c1_rec.request_id;
				
				UPDATE kcrt_request_details
					 SET last_update_date = sysdate,
							 last_updated_by = 1
				 WHERE request_id = c1_rec.request_id;
			 
				UPDATE kcrt_req_header_details
					 SET last_update_date = sysdate,
							 last_updated_by = 1
				 WHERE request_id = c1_rec.request_id;                        

				dbms_output.put_line(c1_rec.request_id || ' - ' || c1_rec.description);                    
			
		END LOOP;

END;
/